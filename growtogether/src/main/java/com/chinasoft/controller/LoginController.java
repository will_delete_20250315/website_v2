/*
 * Copyright (c) 2021 KaiHong Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.chinasoft.annotation.OperationLog;
import com.chinasoft.service.LoginService;
import com.chinasoft.utils.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

/**
 * 登录模块
 *
 * @author dengtao
 */
@CrossOrigin
@RestController
@RequestMapping("/login")
@Api(tags = "登录",description = "登录模块")
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private LoginService loginService;

    @ApiOperation(value = "后台系统登录")
    @GetMapping("/backstageLogin")
    @OperationLog(action = "登录", operateType = "userLogin")
    public ResultVO backstageLogin(@RequestParam("userName") String userName,
                                   @RequestParam("phone") String phone) {
        ResultVO resultVo = new ResultVO();
        try {
            //查询验证账号手机号是否存在
            List<Map<String,Object>> loginList=loginService.getBackstageLogin(userName,phone);
            if(loginList.size()>0){
                resultVo.setCode(resultVo.SUCCESS);
                resultVo.setMsg("登录成功");
                resultVo.setData(loginList);
            }else{
                resultVo.setCode(resultVo.FAIL);
                resultVo.setMsg("登录失败,账号或手机号不存在");
            }
        } catch (Exception e) {
            resultVo.setCode(resultVo.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }
    @ApiOperation(value = "查询中心登录")
    @GetMapping("/centerLogin")
    @OperationLog(action = "登录", operateType = "centerLogin")
    public ResultVO centerLogin(@RequestParam("userName") String userName,
                                @RequestParam("phone") String phone) {
        ResultVO resultVo = new ResultVO();
        try {
            //查询验证账号手机号是否存在
            List<Map<String,Object>> loginList=loginService.centerLogin(userName,phone);
            if(loginList.size()>0){
                resultVo.setCode(resultVo.SUCCESS);
                resultVo.setMsg("登录成功");
                resultVo.setData(loginList);
            }else{
                resultVo.setCode(resultVo.FAIL);
                resultVo.setMsg("登录失败,账号或手机号不存在");
            }
        } catch (Exception e) {
            resultVo.setCode(resultVo.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }
}
