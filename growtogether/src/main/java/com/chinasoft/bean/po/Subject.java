package com.chinasoft.bean.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Subject {

    private int id;
    private String subjectName;
    private String subjectDetails;
    private String facultyProfile;
    private String project;
    private String projectDesc;
    private String difficulty;
    private String outputStandard;
    private String skillsRequirement;
    private String relatedReferences;
    private String questionsSentTo;
    private String mentorContact;
}
