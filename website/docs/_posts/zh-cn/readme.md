---
title: readme
permalink: /pages/extra/9161db/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 导读<a name="ZH-CN_TOPIC_0000001142413608"></a>

此工程存放OpenHarmony提供的快速入门、开发指南、API参考等开发者文档，欢迎参与OpenHarmony开发者文档开源项目，与我们一起完善开发者文档。

## **文档目录结构**<a name="section1976124516374"></a>


- 设备开发
    -   轻量和小型系统开发指导（参考内存<128MB）
        -   overview：[设备开发导读](/pages/extra/006ae2/)
        -   quick-start：[快速入门](/pages/extra/eec566/)（搭建环境、获取源码、编译、烧录等）
        -   Basic Capability：开发基础能力
            -   Kernel：[轻量系统内核](/pages/extra/65dc1e/)
            -   Kernel：[小型系统内核](/pages/extra/80d97b/)
            -   Drivers：[驱动](/pages/extra/551a2e/)
            -   Subsystems：[子系统](/pages/extra/e6eae1/)（编译构建、图形图像、DFX、XTS等子系统）
            -   Security：[隐私与安全](/pages/extra/b51404/)

        -   guide：开发示例
            -   [WLAN连接类产品](/pages/extra/a9f9b7/)（LED外设控制、集成三方SDK）
            -   [无屏摄像头类产品](/pages/extra/b2b869/)（摄像头控制）
            -   [带屏摄像头类产品](/pages/extra/51c9a4/)（屏幕和摄像头控制、视觉应用开发）

        -   porting：移植适配
            -   [轻量和小型系统三方库移植指导](/pages/extra/e80fe1/)
            -   [轻量系统芯片移植指导](/pages/extra/bc2a1e/)
            -   [轻量系统芯片移植案例](/pages/extra/890e18/)
            -   [小型系统芯片移植指导](/pages/extra/362558/)

        -   bundles：HPM Bundle开发
            -   [HPM Bundle开发规范](/pages/01060101)
            -   [HPM Bundle开发指南](/pages/extra/460736/)
            -   [HPM Bundle开发示例](/pages/extra/5aa5a6/)

    -   标准系统开发指导（参考内存≥128MB）
        -   overview：[设备开发导读](/pages/extra/006ae2/)
        -   quick-start：[快速入门](/pages/extra/6dca5d/)（搭建环境、获取源码、编译、烧录等）
        -   Basic Capability：开发基础能力
            -   Kernel：[标准系统内核](/pages/extra/d328cc/)
            -   Drivers：[驱动](/pages/extra/551a2e/)
            -   Subsystems：[子系统](/pages/extra/e6eae1/)（编译构建、图形图像、DFX、XTS等子系统）
            -   Security：[隐私与安全](/pages/extra/b51404/)

        -   guide：开发示例
            -   [时钟应用](/pages/01070201)
            -   [平台驱动](/pages/01070202)
            -   [外设驱动](/pages/01070203)

        -   porting：移植适配
            -   [标准系统芯片移植指导](/pages/01040301)
            -   [一种快速移植OpenHarmony Linux内核的方法](/pages/01040302)

        -   bundles：HPM Bundle开发
            -   [HPM Bundle开发规范](/pages/01060101)
            -   [HPM Bundle开发指南](/pages/extra/460736/)
            -   [HPM Bundle开发示例](/pages/extra/5aa5a6/)
    -   [常见问题](/pages/extra/3a26c5/)


-   应用开发
    -   overview：[应用开发导读](/pages/extra/e59705/)
    -   quick-start：[入门](/pages/extra/82bbc8/)
    -   ability：[Ability框架](/pages/extra/f24f79/)
    -   ui：[UI](/pages/extra/91bbde/)
    -   media：[媒体](/pages/extra/504ad4/)
    -   security：[安全](/pages/extra/85ba77/)
    -   connectivity：[网络与连接](/pages/extra/30f113/)
    -   database：[分布式数据服务](/pages/extra/ebcb6a/)
    -   usb：[USB服务](/pages/extra/6eccf9/)
    -   dfx：[DFX](/pages/extra/0b29a3/)
    -   reference：[开发参考](/pages/extra/23166e/)
-   许可证及版权信息检查工具：[开源合规审查工具](https://gitee.com/openharmony-sig/tools_oat)
-   glossary：[术语](/pages/010103)

## **版本更新**<a name="section945819377382"></a>

参考[Release Notes](/pages/010104)。

## **第三方开源软件及许可说明**<a name="section840310516385"></a>

3rd-Party-License：[第三方开源软件及许可证说明](/pages/extra/ccbaa4/)

## **贡献**<a name="section152287615392"></a>

非常欢迎您参与[贡献](/pages/010d01)，我们鼓励开发者以各种方式参与文档反馈和贡献。

您可以对现有文档进行评价、简单更改、反馈文档质量问题、贡献您的原创内容，详细请参考[贡献文档](/pages/extra/7f2552/)。

卓越贡献者将会在开发者社区文档贡献专栏表彰公示。

