---
title: driver-platform
permalink: /pages/extra/6777d1/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 平台驱动使用<a name="ZH-CN_TOPIC_0000001111199424"></a>

-   **[ADC](/pages/0105020301)**  

-   **[GPIO](/pages/0105020302)**  

-   **[HDMI](/pages/0105020303)**  

-   **[I2C](/pages/0105020304)**  

-   **[I3C](/pages/0105020305)**  

-   **[MIPI-CSI](/pages/0105020306)**  

-   **[MIPI-DSI](/pages/0105020307)**  

-   **[PWM](/pages/0105020308)**  

-   **[RTC](/pages/0105020309)**  

-   **[SDIO](/pages/010502030a)**  

-   **[SPI](/pages/010502030b)**  

-   **[UART](/pages/010502030c)**  

-   **[WATCHDOG](/pages/010502030d)**  
