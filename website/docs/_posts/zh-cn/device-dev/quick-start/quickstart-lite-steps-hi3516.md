---
title: quickstart-lite-steps-hi3516
permalink: /pages/extra/f3d084/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# Hi3516开发板<a name="ZH-CN_TOPIC_0000001217013857"></a>

-   **[安装开发板环境](/pages/010201030201)**  

-   **[新建应用程序](/pages/010201030202)**  

-   **[编译](/pages/010201030203)**  

-   **[烧录](/pages/010201030204)**  

-   **[运行](/pages/010201030205)**  

-   **[常见问题](/pages/010201030206)**  


