---
title: kernel
permalink: /pages/extra/0c607a/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 内核<a name="ZH-CN_TOPIC_0000001111039554"></a>

-   **[轻量系统内核](/pages/extra/65dc1e/)**  

-   **[小型系统内核](/pages/extra/80d97b/)**  

-   **[标准系统内核](/pages/extra/d328cc/)**  


