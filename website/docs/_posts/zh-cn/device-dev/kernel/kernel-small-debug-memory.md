---
title: kernel-small-debug-memory
permalink: /pages/extra/502e79/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 内存调测<a name="ZH-CN_TOPIC_0000001124056309"></a>

-   **[内存信息统计](/pages/01050102050601)**  

-   **[内存泄漏检测](/pages/01050102050602)**  

-   **[踩内存检测](/pages/01050102050603)**  


