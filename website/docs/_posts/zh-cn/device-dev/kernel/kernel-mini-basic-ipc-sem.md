---
title: kernel-mini-basic-ipc-sem
permalink: /pages/extra/9d3f31/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 信号量<a name="ZH-CN_TOPIC_0000001123948077"></a>

-   **[基本概念](/pages/0105010102040401)**  

-   **[开发指导](/pages/0105010102040402)**  


