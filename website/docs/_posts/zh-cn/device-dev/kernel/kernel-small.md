---
title: kernel-small
permalink: /pages/extra/80d97b/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 小型系统内核<a name="ZH-CN_TOPIC_0000001171191693"></a>

-   **[内核概述](/pages/0105010201)**  

-   **[内核启动](/pages/extra/f14e14/)**  

-   **[基础内核](/pages/extra/365208/)**  

-   **[扩展组件](/pages/extra/21b9e6/)**  

-   **[调测与工具](/pages/extra/1d2f34/)**  

-   **[附录](/pages/extra/658691/)**  


