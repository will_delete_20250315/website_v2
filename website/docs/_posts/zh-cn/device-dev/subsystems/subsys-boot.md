---
title: subsys-boot
permalink: /pages/extra/9238e4/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 启动恢复<a name="ZH-CN_TOPIC_0000001157319415"></a>

-   **[启动恢复子系统概述](/pages/01050e01)**  

-   **[init启动引导组件](/pages/01050e02)**  

-   **[appspawn应用孵化组件](/pages/01050e03)**  

-   **[bootstrap服务启动组件](/pages/01050e04)**  

-   **[syspara系统属性组件](/pages/01050e05)**  

-   **[常见问题](/pages/01050e06)**  

-   **[参考](/pages/01050e07)**  


