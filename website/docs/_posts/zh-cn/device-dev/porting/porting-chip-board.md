---
title: porting-chip-board
permalink: /pages/extra/9257d8/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 板级系统移植<a name="ZH-CN_TOPIC_0000001062604739"></a>

-   **[移植概述](/pages/0104010301)**  

-   **[板级驱动适配](/pages/0104010302)**  

-   **[HAL层实现](/pages/0104010303)**  

-   **[系统组件调用](/pages/0104010304)**  

-   **[lwip组件适配](/pages/0104010305)**  

-   **[三方组件适配](/pages/0104010306)**  

-   **[XTS认证](/pages/0104010307)**  


