---
title: porting-chip-prepare
permalink: /pages/extra/2bf51a/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 移植准备<a name="ZH-CN_TOPIC_0000001063252862"></a>

-   **[移植须知](/pages/0104010101)**  

-   **[编译构建适配流程](/pages/0104010102)**  


