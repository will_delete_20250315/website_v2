---
title: ts-gesture-processing
permalink: /pages/extra/74ae35/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 手势处理<a name="ZH-CN_TOPIC_0000001192595144"></a>

-   **[绑定手势方法](/pages/010c020201010301)**  

-   **[基础手势](/pages/extra/51a545/)**  

-   **[组合手势](/pages/010c020201010303)**  


