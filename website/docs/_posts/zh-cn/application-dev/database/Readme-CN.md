---
title: Readme-CN
permalink: /pages/extra/ebcb6a/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 数据管理

- 分布式数据服务
  - [分布式数据服务概述](/pages/01080601)
  - [分布式数据服务开发指导](/pages/01080602)
- 关系型数据库
  - [关系型数据库概述](/pages/extra/75379e/)
  - [分布式数据服务开发指导](/pages/extra/0f5746/)
- 轻量级数据存储
  - [轻量级数据存储概述](/pages/extra/6f0fa4/)
  - [轻量级数据存储开发指导](/pages/extra/46af57/)
