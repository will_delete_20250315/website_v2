---
title: create-openharmony-project
permalink: /pages/extra/4c1a6a/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 创建OpenHarmony工程



- **[使用工程向导创建新工程](/pages/0108090401)**

- **[通过导入Sample方式创建新工程](/pages/0108090402)**