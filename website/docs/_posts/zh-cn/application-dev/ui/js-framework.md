---
title: js-framework
permalink: /pages/extra/8e5cfc/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 框架说明



- **[文件组织](/pages/010802020201)**

- **[js标签配置](/pages/010802020202)**

- **[app.js](/pages/010802020203)**

- **[语法](/pages/extra/54258f/)**

- **[生命周期](/pages/010802020205)**

- **[资源限定与访问](/pages/010802020206)**

- **[多语言支持](/pages/010802020207)**