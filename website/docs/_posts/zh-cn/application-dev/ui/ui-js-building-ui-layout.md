---
title: ui-js-building-ui-layout
permalink: /pages/extra/8c7f97/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 构建布局



- **[布局说明](/pages/01080202030201)**

- **[添加标题行和文本区域](/pages/01080202030202)**

- **[添加图片区域](/pages/01080202030203)**

- **[添加留言区域](/pages/01080202030204)**

- **[添加容器](/pages/01080202030205)**