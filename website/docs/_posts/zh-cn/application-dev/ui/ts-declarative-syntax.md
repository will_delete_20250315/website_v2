---
title: ts-declarative-syntax
permalink: /pages/extra/19a335/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 声明式语法



- **[描述规范使用说明](/pages/010802030301)**

- **[通用UI描述规范](/pages/extra/52abc2/)**

- **[UI状态管理](/pages/extra/09017f/)**

- **[渲染控制语法](/pages/extra/cc10c8/)**

- **[深入理解组件化](/pages/extra/ba7ead/)**

- **[语法糖](/pages/extra/451991/)**