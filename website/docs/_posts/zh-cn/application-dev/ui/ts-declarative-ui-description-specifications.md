---
title: ts-declarative-ui-description-specifications
permalink: /pages/extra/62d804/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 声明式UI描述规范<a name="ZH-CN_TOPIC_0000001157388851"></a>

-   **[无构造参数配置](/pages/0108020303020201)**  

-   **[必选参数构造配置](/pages/0108020303020202)**  

-   **[属性配置](/pages/0108020303020203)**  

-   **[事件配置](/pages/0108020303020204)**  

-   **[子组件配置](/pages/0108020303020205)**  


