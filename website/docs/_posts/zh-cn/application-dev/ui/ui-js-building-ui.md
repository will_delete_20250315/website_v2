---
title: ui-js-building-ui
permalink: /pages/extra/7184fa/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 构建用户界面



- **[组件介绍](/pages/010802020301)**

- **[构建布局](/pages/extra/8c7f97/)**

- **[添加交互](/pages/010802020303)**

- **[动画](/pages/010802020304)**

- **[事件](/pages/010802020305)**

- **[页面路由](/pages/010802020306)**
