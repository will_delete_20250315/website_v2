---
title: audio
permalink: /pages/extra/6d22e7/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 音频


- **[音频开发概述](/pages/0108030101)**

- **[音频播放开发指导](/pages/0108030102)**

- **[音频管理开发指导](/pages/0108030103)**

- **[音频录制开发指导](/pages/0108030104)**