---
title: 兼容性认证服务
permalink: /xts
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-16 00:50:12
---
# 1. 简介

OpenHarmony兼容性测试主要是验证合作伙伴的设备和业务应用满足OpenHarmony开源兼容性定义的技术要求，确保运行在OpenHarmony上的设备和业务应用能稳定、正常运行，同时使用OpenHarmony的设备和业务应用有一致性的接口和业务体验。

OpenHarmony兼容性测试需要合作伙伴获取代码和兼容性测试套，并完成自测，取得兼容性测试报告后，在2 认证流程概述上传兼容性测试报告供开放原子开源基金会团队进行审核或抽测，本版块是兼容性测试指导详细介绍。
​
兼容性测试的套件范围：

ACTS（application compatibility test suite）应用兼容性测试套件，看护北向APP兼容、OpenHarmony开发API兼容。    

其余套件的发布敬请期待。

# 2. 兼容性测试LTS版本要求
兼容性测试套件版本说明：

OpenHarmony兼容性测试套件需要使用LTS（Long Term Support）版本，目前有2个版本LTS版本，打Tag点的OpenHarmony_release_v1.1.0、OpenHarmony-v1.1.1-LTS。LTS有时效性限制，提交认证时，需要使用12个月内发布的版本。

兼容性测试套件LTS版本的获取方式：

进入 [XTS仓库](https://gitee.com/openharmony/xts_acts) 。

![](/services/Picture1.svg)

# 3. 兼容性认证设备类型介绍
OpenHarmony支持如下几种设备类型：
### 轻量系统类设备（参考内存≥128KB）

面向MCU类处理器，例如Arm Cortex-M、RISC-V 32位的设备，资源极其有限，参考内存≥128KB，提供丰富    的近距连接能力以及丰富的外设总线访问能力。典型产品有智能家居领域的联接类模组、传感器设备等。联接类模组通常应用在智能物联网设备中，负责实现联接部分的硬件模块，在智能家居领域由厂家集成到其设备中。例如：联接类模组提供WLAN/Bluetooth的接入和数据的联接，模组与厂家家居的芯片通常通过UART或GPIO等总线接口进行通信。

### 3.1 小型系统类设备（参考内存≥1MB）

面向应用处理器，例如Arm Cortex-A的设备，参考内存≥1MB，提供更高的安全能力，提供标准的图形框架，提供视频编解码的多媒体能力。典型产品有智能家居领域的IPCamera、电子猫眼、路由器以及智慧出行域的行车记录仪等。

### 3.2 标准系统类设备（参考内存≥128MB）

面向应用处理器，例如Arm Cortex-A的设备，参考内存≥128MB，提供增强的交互能力，提供3D GPU以及硬件合成能力，提供更多控件以及动效更丰富的图形能力，提供完整的应用框架。典型产品有高端的冰箱显示屏等。

# 4. 认证流程概述

本文档主要面向OpenHarmony开源基金会设备开发合作伙伴，指导合作伙伴如何进行设备兼容性自测试，以及在自测试基础上进行OpenHarmon设备认证的流程。

![](/services/image005.png)
 OpenHarmony兼容性测试主要步骤如下
* 步骤 1开发者从[gitee获取OpenHarmony源码](https://gitee.com/openharmony)
* 步骤 2开发者获取测试框架和工具，见兼容性测试指导
* 步骤 3开发者本地测试执行兼容性测试套件
* 步骤 4开发者上传自测试报告到[OpenHarmony兼容性测试网站](https://www.openharmony.cn/old/#/Compatibility_test)
* 步骤 5开源基金会审核开发者上传的测试报告，给出审核结果，若审批通过，到步骤8；不通过，到步骤6
* 步骤 6开源基金会启动抽检流程，上传测试套件包到网站
* 步骤 7开发者下载抽检测试套件包，并在本地执行，生成测试报告后上传到[OpenHarmony兼容性测试网站](https://www.openharmony.cn/old/#/Compatibility_test)
* 步骤 8测试报告审核通过后，OpenHarmony兼容性测试通过

# 5. 兼容性工作组文档
- [兼容性工作组章程](https://www.openharmony.cn/attachments/compatibility/rules.pdf)
- [兼容性工作组文档仓](https://gitee.com/openharmony-sig/compatibility)
- [兼容性工作组代码仓](https://gitee.com/openharmony/xts_acts)
- [OpenHarmony产品兼容性规范V3.0（PCS-V3.0）](https://downloads.openharmony.cn/files/OpenHarmony3.0-PCS.pdf)