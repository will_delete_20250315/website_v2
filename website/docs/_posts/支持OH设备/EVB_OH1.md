---
title: EVB_OH1
permalink: /supported_devices/EVB_OH1
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---
# EVB_OH1

<img src="/devices/EVB_OH1/figures/front.jpg"> 



# 芯片参数

| 名称 | 参数                            |
| ---- | ------------------------------- |
| Soc  | T507                            |
| CPU  | 4x ARM®Cortex-A53 up to 1.5 GHz |
| GPU  | Mali G31                        |
| RAM  | 4096 Mbyte DDR3L                |
| ROM  | eMMC 32 Gbyte micro             |



# 外设

| 名称          | 参数                                                         |
| ------------- | ------------------------------------------------------------ |
| Ethernet Port | 2 x 10/100/1000 Mbit/s, IEEE 802.3 Compliant                 |
| Wireless      | WiFi: 2.4GHz IEEE 802.11 b/g/n<br>Bluetooth® v4.2<br>XR829芯片 |
| USB           | Host:3xUSB 2.0 high-speed<br/>  OTG: 1x USB 2.0 high-speed   |
| Display       | LVDS、CVBS、RGB、HDMI 2.0                                    |
| Audio         | 1路                                                          |
| UART          | 6路                                                          |
| I2C           | 6路                                                          |
| SPI           | 2路                                                          |
| SDIO3.0       | 1路                                                          |
| Power Source  | DC 5V   <2W                                                  |



# 其他

- openharmony 兼容性认证（进行中）
- [Software](https://gitee.com/openharmony-sig/devboard_device_allwinner_t507)
- [Hardware](https://gitee.com/iotclub/evb_oh1_board.git)
- [Support](https://gitee.com/openharmony-sig/devboard_device_allwinner_t507/issues)
