---
title: BL-HWC-G1
permalink: /supported_devices/BL_HWC_G1
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---


# BL-HWC-G1
<img src="/devices/BL_HWC_G1/figures/front.png">

# 芯片参数

| 名称     | 参数                                                         |
| -------- | ------------------------------------------------------------ |
| Soc      | BL602C                                                       |
| CPU      | 32bit RISCV CPU 包含用于 32 位单精度算术的 FPU（浮点单元）<br/>时钟频率 192MHz<br/>CPU性能约 1.46 DMIPS / MHz；3.1 CoreMark / MHz |
| RAM      | 276KB SRAM                                                   |
| ROM      | 128KB ROM <br/>1Kb eFuse <br/>嵌入式 Flash 闪存 (选配)       |
| Wireless | 支持 IEEE 802.11 b/g/n 协议 <br/>2.4 GHz 频带 1T1R 模式，支持 20 MHz，数据速率高达 72.2 Mbps <br/>Wi-Fi 安全 WPS/WEP/WPA/WPA2 Personal/WPA2 Enterprise/WPA3 <br/>无线多媒体 (WMM) <br/>帧聚合 (AMPDU,AMSDU) <br/>立即块回复 (Immediate Block ACK) <br/>分片和重组 (Fragmentation and defragmenta-tion) <br/>Beacon 自动接收 (硬件 TSF) <br/>硬件支持 6 × 虚拟 Wi-Fi 接口 <br/>支持 Station + BLE 模式、Station + SoftAP + BLE 模 式<br/>支持多个云同时接入 <br/>集成 balun，PA/LNA <br/>Bluetooth 低能耗 5.0，Bluetooth Mesh <br/>BLE 协助实现 Wi-Fi 快速连接 <br/>Wi-Fi 和 BLE 共存 <br/>支持 BLE 5.0 通道选择＃ 2 <br/>不支持 2M PHY /编码 PHY / ADV |

# 外设

| 名称                | 参数                                             |
| ------------------- | ------------------------------------------------ |
| SDIO                | 1 路 2.0 从机                                    |
| SPI                 | 1 路 SPI 主/从机，最高速度可达 40Mbps            |
| UART                | 2 路  最高波特率可达 10Mbps, 支持 RTS/CTS 流控   |
| I2C                 | 1 路， 主机，最高速速度可达 3Mbps <br/>          |
| PWM                 | 5 路 ，最高输出频率可达 40MHz <br/>              |
| DAC                 | 2 路 10-bit 通用，最高转换速度可达 512Ksps <br/  |
| ADC                 | 12 路 12-bit 通用 ，最高转换速度可达 2Msps <br/> |
| 模拟比较器（ACOMP） | 2 路通用，可作为 CPU 睡眠唤醒源 <br/>            |
| PIR                 | 1 路 ，可作为 CPU 睡眠唤醒源 <br/>               |
| Infrared Remote     | 1 路，支持 NEC RC5 协议 <br/>                    |
| GPIO                | 16 或 23 个                                      |

# 电源

| 名称         | 参数                                                         |
| ------------ | ------------------------------------------------------------ |
| 电源管理模式 | 关闭  <br/>休眠（多种模式可配） <br/>掉电睡眠（多种模式可配） <br/>正常运作 |

# 时钟

| 名称     | 参数                                                         |
| -------- | ------------------------------------------------------------ |
| 时钟架构 | 支持外部晶振频率 24/32/38.4/40MHz <br/>内置 RC 32kHz 振荡器 <br/>内置 RC 32MHz 振荡器 <br/>内置系统 PLL <br/>支持外部 XTAL 32kHz |

#  其他

OpenHarmony 兼容性认证（进行中）

* [Software](https://gitee.com/openharmony-sig/device_bouffalolab)
* [Hardware](https://dev.bouffalolab.com/hardware)
* [Support](https://zulip.openharmony.cn/#narrow/stream/7-devboard_sig)
