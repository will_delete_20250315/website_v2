---
title: GenkiPi
permalink: /supported_devices/GenkiPi
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---
# GenkiPi

正面
<img src="/devices/GenkiPi/figures/GenkiPi/front.png">
背面

<img src="/devices/GenkiPi/figures/GenkiPi/back.png">

# 芯片参数

| 名称 | 参数                   |
| ---- | ---------------------- |
| Soc  | Hi3861V100高效能芯片组 |
| CPU  | 主频160MHz             |
| SRAM | 352KB                  |
| ROM  | 288KB, 2M Flash        |



# 外设

| 名称         | 参数                             |
| ------------ | -------------------------------- |
| Wireless     | WiFi:  2.4GHz IEEE 802.11 b/g/n  |
| UART         | 3路                              |
| I2C          | 2路                              |
| SPI          | 2路                              |
| GPIO         | 15路                             |
| SDIO         | 1路                              |
| ADC          | 7路                              |
| PWM          | 6路                              |
| I2S          | 1路                              |
| Power Source | USB type-c供电或DC 5v - 16v 供电 |

(注: 上述接口通过复用实现)

# 其他

* OpenHarmony 兼容性认证（通过）
* [Software](https://gitee.com/openharmony-sig/devboard_device_itcast_genkipi)              

* [Support](https://gitee.com/openharmony-sig/devboard_device_itcast_genkipi/issues)