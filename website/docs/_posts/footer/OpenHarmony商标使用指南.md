---
title: 商标使用指南
date: 2021-07-12 17:03:28
permalink: /tradeMark
categories:
tags:
sidebar: false
article: false
comment: false
editLink: false
---
版本号: 1.0     发布时间: 2021年6月1日

&ensp;&ensp;&ensp;&ensp;开放原子开源基金会享有OpenHarmony商标权益。以下为第三方正确使用OpenHarmony商标的指南（注：错误用法示例不视为开放原子开源基金会放弃相应的商标权利）。

# 允许使用OpenHarmony商标的情形

1. 合理使用：如果对商标的使用是指示性的——即使用商标进行符合事实的陈述，可以不经许可，按以下第二条规定的使用规范来使用OpenHarmony商标。合理使用包括如下情形：

    * 第一，在您自己的网站上设置跳转OpenHarmony项目的超链接时，使用OpenHarmony商标；
    * 第二，在非商业用途的宣传材料（如演示文档）中为介绍OpenHarmony开源项目相关事实而使用OpenHarmony商标。

&ensp;&ensp;&ensp;&ensp;请注意，前述合理使用行为不应容易导致相关公众对于开放原子开源基金会和/或OpenHarmony开源项目与您之间的关系产生误解，包括不应明示或暗示开放原子开源基金会及OpenHarmony项目群工作委员会赞助、认可或支持您的公司、产品或服务。
    * 错误用法示例：在OpenHarmony™项目群工作委员会的推动下，XXX产品全面升级。

2. 经许可后使用：如您希望在您的产品或服务中、在公司或机构名称中、在会议或活动名称中、在出版物、宣传材料（用于商业目的）、网站、域名中使用OpenHarmony商标，应向开放原子开源基金会提交使用许可申请。
如需获得OpenHarmony商标使用许可，您可以发送电子邮件到legal@openatom.org，邮件主题应为“OpenHarmony商标使用申请”。请确保在邮件正文中包含如下信息：

    * 商标使用人名称
    * 商标使用目的（商业/非商业）
    * 拟使用方式
    * 拟使用场景

# OpenHarmony商标使用规范

1. OpenHarmony商标的标准式样请通过访问如下链接获取（任何缩放，必须保留原有的比例）：
（标记TM版）
[OpenHarmony™](https://obs.openharmony.cn/f93826b8-3b72-4d17-8679-c606a94e1f33.svg "OpenHarmony™")
（未标记TM版）
[OpenHarmony](https://obs.openharmony.cn/242691b5-b5dc-4d1b-97cf-5ac358bebad4.svg "OpenHarmony")

2. 在各次使用OpenHarmony商标的相应场景中，比如每篇宣传材料中，第一次提及OpenHarmony商标时，OpenHarmony应加角标TM，表示作为商标使用，即OpenHarmony™。
3. 以准确形式使用OpenHarmony商标，OpenHarmony应始终保持“Open”和“Harmony”首字母大写、其余字母小写的形式，且不得使用复数或所有格形式。另外，既不缩写或使用连字符，也不与任何其他字母或单词相结合。

    * 错误用法示例：OPENHARMONY™
    * 错误用法示例：openharmony™
    * 错误用法示例：OPENharmony™
    * 错误用法示例：openHARMONY™
    * 错误用法示例：OpenHarmony's™
    * 错误用法示例：OpenHarmonies™
    * 错误用法示例：Open-Harmony™
    * 错误用法示例：OH-OpenHarmony™
    * 错误用法示例：OH 

4. 在各使用场景中，需包含如下声明，以承认OpenHarmony™是开放原子开源基金会所拥有的商标。
    声明：OpenHarmony™是开放原子开源基金会所拥有的商标。

# 禁止使用OpenHarmony商标的情形

1. 禁止以任何与本指南要求不符的方式使用OpenHarmony商标。
2. 禁止以任何误导或贬损的方式使用OpenHarmony商标，如在虚假广告、虚假/误导性宣传材料中使用OpenHarmony商标。
3. 禁止未经授权使用OpenHarmony商标或与OpenHarmony商标容易混淆的标识。

    * 错误用法示例：OHarmony
    * 错误用法示例：HarmonyOpen

&ensp;&ensp;&ensp;&ensp;开放原子开源基金会可能不定期更新OpenHarmony商标使用指南。您应当在实际使用OpenHarmony商标前访问OpenHarmony项目官方网站，以确认最新版本的OpenHarmony商标使用指南。

