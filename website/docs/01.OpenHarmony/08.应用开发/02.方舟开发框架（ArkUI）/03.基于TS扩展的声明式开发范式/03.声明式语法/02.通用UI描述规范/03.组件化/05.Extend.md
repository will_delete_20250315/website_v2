---
title: Extend
permalink: /pages/0108020303020305
navbar: true
sidebar: true
prev: true
next: true
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:27
---
# @Extend<a name="ZH-CN_TOPIC_0000001134698822"></a>

**@Extend**装饰器将新的属性函数添加到内置组件上，如**Text**、**Column**、**Button**等。通过**@Extend**装饰器可以快速定义并复用组件的自定义样式。

```
@Extend(Text) function fancy(fontSize: number) {
  .fontColor(Color.Red)
  .fontSize(fontSize)
  .fontStyle(FontStyle.Italic)
}

@Entry
@Component
struct FancyUse {
  build() {
    Row({ space: 10 }) {
      Text("Fancy")
        .fancy(16)
      Text("Fancy")
        .fancy(24)
    }
  }
}
```

>![icon-note.gif](/images/application-dev/ui/public_sys-resources/icon-note.gif) **说明：** 
>@Extend装饰器不能用在自定义组件struct定义框内。

