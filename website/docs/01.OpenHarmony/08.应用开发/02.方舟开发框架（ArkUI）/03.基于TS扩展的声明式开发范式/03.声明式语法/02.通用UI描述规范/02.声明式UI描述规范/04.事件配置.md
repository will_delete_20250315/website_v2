---
title: 事件配置
permalink: /pages/0108020303020204
navbar: true
sidebar: true
prev: true
next: true
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:27
---
# 事件配置<a name="ZH-CN_TOPIC_0000001157388863"></a>

通过事件方法可以配置组件支持的事件。

-   使用lambda表达式配置组件的事件方法：

    ```
    // Counter is a private data variable defined in the component.
    Button('add counter')
        .onClick(() => {
            this.counter += 2
        })
    ```


-   使用匿名函数表达式配置组件的事件方法，要求使用**bind**，以确保函数体中的this引用包含的组件。

    ```
    // Counter is a private data variable defined in the component.
    Button('add counter')
        .onClick(function () {
            this.counter += 2
        }.bind(this))
    ```


-   使用组件的成员函数配置组件的事件方法：

    ```
    myClickHandler(): void {
        // do something
    }
    
    ...
    
    Button('add counter')
      .onClick(this.myClickHandler)
    ```


