---
title: 文档中涉及到的内置枚举值
permalink: /pages/010c02020401
navbar: true
sidebar: true
prev: true
next: true
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 文档中涉及到的内置枚举值<a name="ZH-CN_TOPIC_0000001237715093"></a>

## Alignment枚举说明<a name="section1145418513159"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.2"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>TopStart</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>顶部起始端。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Top</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>顶部横向居中。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>TopEnd</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>顶部尾端。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Start</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>起始端纵向居中。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Center</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>横向和纵向居中。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>End</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>尾端纵向居中。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>BottomStart</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>底部起始端。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Bottom</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>底部横向居中。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>BottomEnd</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>底部尾端。</p>
</td>
</tr>
</tbody>
</table>

## Axis枚举说明<a name="section1397918486200"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="25.2%" id="mcps1.1.3.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="74.8%" id="mcps1.1.3.1.2"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="25.2%" headers="mcps1.1.3.1.1 "><p>Vertical</p>
</td>
<td class="cellrowborder" valign="top" width="74.8%" headers="mcps1.1.3.1.2 "><p>方向为纵向。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.2%" headers="mcps1.1.3.1.1 "><p>Horizontal</p>
</td>
<td class="cellrowborder" valign="top" width="74.8%" headers="mcps1.1.3.1.2 "><p>方向为横向。</p>
</td>
</tr>
</tbody>
</table>

## ItemAlign枚举说明<a name="section14211428171612"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="37.54%" id="mcps1.1.3.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="62.46000000000001%" id="mcps1.1.3.1.2"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="37.54%" headers="mcps1.1.3.1.1 "><p>Auto</p>
</td>
<td class="cellrowborder" valign="top" width="62.46000000000001%" headers="mcps1.1.3.1.2 "><p>使用Flex容器中默认配置。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="37.54%" headers="mcps1.1.3.1.1 "><p>Start</p>
</td>
<td class="cellrowborder" valign="top" width="62.46000000000001%" headers="mcps1.1.3.1.2 "><p>元素在Flex容器中，交叉轴方向首部对齐。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="37.54%" headers="mcps1.1.3.1.1 "><p>Center</p>
</td>
<td class="cellrowborder" valign="top" width="62.46000000000001%" headers="mcps1.1.3.1.2 "><p>元素在Flex容器中，交叉轴方向居中对齐。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="37.54%" headers="mcps1.1.3.1.1 "><p>End</p>
</td>
<td class="cellrowborder" valign="top" width="62.46000000000001%" headers="mcps1.1.3.1.2 "><p>元素在Flex容器中，交叉轴方向底部对齐。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="37.54%" headers="mcps1.1.3.1.1 "><p>Stretch</p>
</td>
<td class="cellrowborder" valign="top" width="62.46000000000001%" headers="mcps1.1.3.1.2 "><p>元素在Flex容器中，交叉轴方向拉伸填充，在未设置尺寸时，拉伸到容器尺寸。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="37.54%" headers="mcps1.1.3.1.1 "><p>Baseline</p>
</td>
<td class="cellrowborder" valign="top" width="62.46000000000001%" headers="mcps1.1.3.1.2 "><p>元素在Flex容器中，交叉轴方向文本基线对齐。</p>
</td>
</tr>
</tbody>
</table>

## LineCapStyle枚举说明<a name="section549694781614"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="25.2%" id="mcps1.1.3.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="74.8%" id="mcps1.1.3.1.2"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="25.2%" headers="mcps1.1.3.1.1 "><p>Butt</p>
</td>
<td class="cellrowborder" valign="top" width="74.8%" headers="mcps1.1.3.1.2 "><p>分割线两端为平行线。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.2%" headers="mcps1.1.3.1.1 "><p>Round</p>
</td>
<td class="cellrowborder" valign="top" width="74.8%" headers="mcps1.1.3.1.2 "><p>分割线两端为半圆。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="25.2%" headers="mcps1.1.3.1.1 "><p>Square</p>
</td>
<td class="cellrowborder" valign="top" width="74.8%" headers="mcps1.1.3.1.2 "><p>分割线两端为平行线。</p>
</td>
</tr>
</tbody>
</table>

## PlayMode枚举值说明<a name="section2780178121714"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.2"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Normal</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>动画按正常播放。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Reverse</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>动画反向播放。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Alternate</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>动画在奇数次（1、3、5...）正向播放，在偶数次（2、4、6...）反向播放。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>AlternateReverse</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>动画在奇数次（1、3、5...）反向播放，在偶数次（2、4、6...）正向播放。</p>
</td>
</tr>
</tbody>
</table>

## ImageRepeat枚举说明<a name="section5656191941718"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.2"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>X</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>只在水平轴上重复绘制图片。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Y</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>只在竖直轴上重复绘制图片。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>XY</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>在两个轴上重复绘制图片。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>NoRepeat</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>不重复绘制图片。</p>
</td>
</tr>
</tbody>
</table>

## TextDecorationType枚举说明<a name="section81063305178"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.2"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Underline</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>文字下划线修饰。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>LineThrough</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>穿过文本的修饰线。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Overline</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>文字上划线修饰。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>None</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>不使用文本装饰线。</p>
</td>
</tr>
</tbody>
</table>

## TextCase枚举说明<a name="section1276124151715"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.2"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Normal</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>保持文本原有大小写。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>LowerCase</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>文本采用全小写。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>UpperCase</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>文本采用全大写。</p>
</td>
</tr>
</tbody>
</table>

## BarState枚举说明<a name="section122549529179"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="50%" id="mcps1.1.3.1.2"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Off</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>不显示。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>On</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>常驻显示。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p>Auto</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p>按需显示(触摸时显示，2s后消失)。</p>
</td>
</tr>
</tbody>
</table>

