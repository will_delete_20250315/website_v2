---
title: 搭建环境
permalink: /pages/01050a02
navbar: true
sidebar: true
prev: true
next: true
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:25
---
# 搭建环境<a name="ZH-CN_TOPIC_0000001061629245"></a>

-   开发板：Hi3516DV300

-   [下载源码](/pages/extra/51fbe5/)
-   [编译用户程序框架](/pages/01010219)

