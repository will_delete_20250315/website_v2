/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.chinasoft.annotation.OperationLog;
import com.chinasoft.bean.po.BusType;
import com.chinasoft.service.BusTypeService;
import com.chinasoft.service.BusTypeService;
import com.chinasoft.utils.PageListVO;
import com.chinasoft.utils.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 业务类型模块
 *
 * @author dengtao
 */
@CrossOrigin
@RestController
@RequestMapping("/busType")
@Api(tags = "业务类型", description = "业务类型管理")
public class BusTypeController {

    private static final Logger logger = LoggerFactory.getLogger(BusTypeController.class);

    @Autowired
    private BusTypeService busTypeService;

    @ApiOperation(value = "业务类型信息保存")
    @PostMapping("/addBusType")
    @OperationLog(action = "业务类型信息[保存]", operateType = "busType/save")
    public ResultVO save(@RequestBody BusType busType) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null != busType) {
                BusType busTypeVO = busTypeService.find(busType);
                if (null != busTypeVO) {
                    resultVo.setCode(ResultVO.FAIL);
                    resultVo.setMsg("业务类型名称已存在");
                    return resultVo;
                }
            }
            int res = busTypeService.add(busType);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("新增成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }


    @ApiOperation(value = "业务类型信息修改")
    @PostMapping("/editBusType")
    @OperationLog(action = "业务类型信息[修改]", operateType = "busType/edit")
    public ResultVO edit(@RequestBody BusType busType) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == busType) {
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("参数为空");
                return resultVo;
            }
            int res = busTypeService.edit(busType);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("编辑成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "业务类型信息删除")
    @PostMapping("/delBusType")
    @OperationLog(action = "业务类型信息[删除]", operateType = "busType/remove")
    public ResultVO remove(@RequestBody BusType busType) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == busType || busType.getBusTypeId() == 0) {
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("参数为空");
                return resultVo;
            }
            int res = busTypeService.remove(busType.getBusTypeId());
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("删除成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "业务类型信息批量删除")
    @GetMapping("/removeBatch")
    @OperationLog(action = "业务类型信息[批量删除]", operateType = "busType/removeBatch")
    public ResultVO removeBatch(@RequestParam(value = "ids", required = false) String ids) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == ids || "".equals(ids)) {
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("ids or proIds is null");
                return resultVo;
            }
            int res = busTypeService.removeBatch(ids);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("remove batch busType success");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "业务类型信息单个查看")
    @GetMapping("/find")
    @OperationLog(action = "业务类型信息[单个查看]", operateType = "busType/find")
    public ResultVO find(BusType busType) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == busType) {
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("param is null");
                return resultVo;
            }
            BusType busTypeVO = busTypeService.find(busType);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("find busType success");
            resultVo.setData(busTypeVO);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    /**
     * 分页查询
     *
     * @param busType
     * @return pageListVO
     */
    @ApiOperation(value = "业务类型信息分页查询")
    @GetMapping("/queryBusType")
    @OperationLog(action = "业务类型信息[分页查询]", operateType = "busType/findBatch")
    public PageListVO<List<BusType>> findBatch(BusType busType) {
        PageListVO<List<BusType>> pageListVO = new PageListVO<>();
        if (null == busType) {
            busType = new BusType();
        }
        if (null == busType.getPageNum() || busType.getPageNum() <= 0) {
            busType.setPageNum(1);
        }
        if (null == busType.getPageSize()) {
            busType.setPageSize(10);
        }
        List<BusType> resultList = busTypeService.findBatch(busType);
        Integer resultTotal = busTypeService.findTotal(busType);
        int page = resultTotal / busType.getPageSize() + (resultTotal % busType.getPageSize() > 0 ? 1 : 0);
        pageListVO.setTotalPage(page);
        pageListVO.setData(resultList);
        pageListVO.setCode(PageListVO.SUCCESS);
        pageListVO.setTotalNum(resultTotal);
        pageListVO.setPageNum(busType.getPageNum());
        pageListVO.setPageSize(busType.getPageSize());
        return pageListVO;
    }

    @ApiOperation(value = "业务类型信息单个查看")
    @GetMapping("/getAllData")
    @OperationLog(action = "业务类型信息[单个查看]", operateType = "busType/find")
    public ResultVO getAllData() {
        ResultVO resultVo = new ResultVO();
        resultVo.setData(busTypeService.getAllData());
        resultVo.setCode(ResultVO.SUCCESS);
        resultVo.setMsg("查询成功");
        return resultVo;
    }
}
