/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.alibaba.fastjson.JSONObject;
import com.chinasoft.annotation.OperationLog;
import com.chinasoft.service.FrontPageService;
import com.chinasoft.utils.PageListVO;
import com.chinasoft.utils.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;

/**
 * 知识图谱首页
 * @author dengtao
 */

@CrossOrigin
@RestController
@RequestMapping("/frontPage")
@Api(tags = "首页",description = "知识图谱首页")
public class FrontPageController {

    private static final Logger logger = LoggerFactory.getLogger(FrontPageController.class);

    @Autowired
    private FrontPageService frontPageService;

    @ApiOperation(value = "首页初始化数据查询")
    @GetMapping("/queryInitialization")
    @OperationLog(action = "查询", operateType = "query")
    public PageListVO queryInitialization(@RequestParam("pageSize") int pageSize,
                                          @RequestParam("pageNum") int pageNum,
                                          @RequestParam(value = "titleId",required = false)String titleId,
                                          @RequestParam(value = "osTypeId",required = false)String osTypeId,
                                          @RequestParam(value = "osVersionId",required = false)String osVersionId,
                                          //@RequestParam(value = "levelId",required = false)String levelId,
                                          @RequestParam(value = "busTypeId",required = false)String busTypeId,
                                          @RequestParam(value = "featuresId",required = false)String featuresId) {
        PageListVO pageListVO = new PageListVO();
        try {
            if (pageSize != 9) {
                pageListVO.setCode(1);
                pageListVO.setMsg("页码大小错误");
            } else if (pageNum < 1) {
                pageListVO.setCode(1);
                pageListVO.setMsg("请求页码错误");
            } else {
                HashMap<String, String> params = new HashMap<>();
                params.put("pageSize", pageSize + "");
                int start = (pageNum - 1) * pageSize;
                params.put("start", start + "");
                params.put("pageNum", pageNum + "");
                params.put("titleId",titleId);
                //params.put("levelId",levelId);
                params.put("osTypeId",osTypeId);
                params.put("osVersionId",osVersionId);
                params.put("busTypeId",busTypeId);
                params.put("featuresId",featuresId);
                JSONObject json=frontPageService.getFrontPageData(params);
                pageListVO.setPageNum(pageNum);
                pageListVO.setPageSize(pageSize);
                //总条数
                pageListVO.setTotalNum(Integer.parseInt(json.get("totalNum").toString()));
                //余下条数
                pageListVO.setTotalPage(Integer.parseInt(json.get("totalPage").toString()));
                json.remove("totalPage");
                json.remove("totalNum");
                pageListVO.setCode(PageListVO.SUCCESS);
                pageListVO.setMsg("查询成功");
                pageListVO.setData(json);
            }
        } catch (Exception e) {
            pageListVO.setCode(PageListVO.FAIL);
            pageListVO.setMsg(e.getMessage());
            e.printStackTrace();
        }
        return pageListVO;
    }

    @ApiOperation(value = "首页开发样例详情查询")
    @GetMapping("/querySampleDetail")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO querySampleDetail(@RequestParam("sampleId") String sampleId) {
        ResultVO resultVo = new ResultVO();
        try {
            resultVo.setData(frontPageService.getFrontPageDetailData(sampleId));
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }
}
