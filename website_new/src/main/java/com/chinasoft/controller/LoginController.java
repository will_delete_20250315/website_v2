/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.chinasoft.annotation.OperationLog;
import com.chinasoft.bean.po.User;
import com.chinasoft.constants.KnowledgeConstants;
import com.chinasoft.service.LoginService;
import com.chinasoft.utils.MD5;
import com.chinasoft.utils.ResultVO;
import com.chinasoft.utils.TokenUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 登录模块
 *
 * @author dengtao
 */
@CrossOrigin
@RestController
@RequestMapping("/login")
@Api(tags = "登录",description = "登录模块")
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private LoginService loginService;

    @ApiOperation(value = "后台系统登录")
    @PostMapping("/backstageLogin")
    @OperationLog(action = "登录", operateType = "userLogin")
    public ResultVO backstageLogin(@RequestBody User user) {
        ResultVO resultVo = new ResultVO();
        try {
            if (StringUtils.isEmpty(user.getUserName()) || StringUtils.isEmpty(user.getPassword())) {
                resultVo.setCode(ResultVO.FAIL);
                logger.error("userName or password is null!");
                resultVo.setMsg("参数不能为空");
            }

            String pwd = MD5.string2MD5(user.getPassword());
            //查询验证账号手机号是否存在
            List<Map<String,Object>> loginList = loginService.getBackstageLogin(user.getUserName(),pwd);
            if(loginList.size() > 0){
                String token = TokenUtils.token(user.getUserName(),user.getPassword() ,"app");
                User user1 = new User();
                user1.setId(Integer.parseInt(String.valueOf(loginList.get(0).get("id"))));
                user1.setToken(token);
                user1.setLoginTime(new Date());
                loginService.updateUser(user1);

                User resUser = new User();
                resUser.setUserName(user.getUserName());
                resUser.setToken(token);
                resUser.setLoginTime(new Date());
                resultVo.setCode(ResultVO.SUCCESS);
                resultVo.setMsg("登录成功");
                resultVo.setData(resUser);
            }else{
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("登录失败,账号或密码不存在");
            }
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }
}
