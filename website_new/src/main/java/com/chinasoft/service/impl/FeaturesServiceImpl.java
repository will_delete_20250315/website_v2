/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service.impl;

import com.chinasoft.bean.po.Features;
import com.chinasoft.bean.vo.QuerySampleVo;
import com.chinasoft.mapper.FeaturesMapper;
import com.chinasoft.service.FeaturesService;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FeaturesServiceImpl implements FeaturesService {

    private static Logger logger = Logger.getLogger(FeaturesServiceImpl.class);

    @Autowired
    private FeaturesMapper featuresMapper;

    @Override
    public int add(Features features) {
        return featuresMapper.insert(features);
    }

    @Override
    public int edit(Features features) {
        return featuresMapper.update(features);
    }

    @Override
    public int remove(Integer id) {
        if (null == id){
            logger.error("id is null");
            return 0;
        }
        return featuresMapper.delete(id);
    }

    @Override
    public int removeBatch(String ids) {
        if (StringUtils.isEmpty(ids) && StringUtils.isEmpty(ids)){
            logger.error("ids or proIds is null");
            return 0;
        }
        List<Integer> idsList = new ArrayList<>();
        if (!StringUtils.isEmpty(ids)){
           idsList = Lists.newArrayList(ids.split(",")).stream().map(id->Integer.parseInt(id)).collect(Collectors.toList());
        }
        return featuresMapper.deleteBatch(idsList);
    }

    @Override
    public Features find(Features features) {
        return featuresMapper.query(features);
    }


    @Override
    public List<Features> findAll(Features features) {
        return featuresMapper.queryAll(features);
    }

    @Override
    public List<QuerySampleVo> findBatch(Features features) {
        return featuresMapper.queryBatch(features);
    }

    @Override
    public int findTotal(Features features) {
        return featuresMapper.queryTotal(features);
    }
}
