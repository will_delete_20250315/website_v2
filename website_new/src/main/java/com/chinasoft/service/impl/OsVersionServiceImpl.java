/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service.impl;

import com.chinasoft.bean.po.OsVersion;
import com.chinasoft.mapper.OsVersionMapper;
import com.chinasoft.service.OsVersionService;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OsVersionServiceImpl implements OsVersionService {

    private static Logger logger = Logger.getLogger(OsVersionServiceImpl.class);

    @Autowired
    private OsVersionMapper osVersionMapper;

    @Override
    public int add(OsVersion osVersion) {
        return osVersionMapper.insert(osVersion);
    }

    @Override
    public int edit(OsVersion osVersion) {
        return osVersionMapper.update(osVersion);
    }

    @Override
    public int remove(Integer id) {
        if (null == id){
            logger.error("id is null");
            return 0;
        }
        return osVersionMapper.delete(id);
    }

    @Override
    public int removeBatch(String ids) {
        if (StringUtils.isEmpty(ids) && StringUtils.isEmpty(ids)){
            logger.error("ids or proIds is null");
            return 0;
        }
        List<Integer> idsList = new ArrayList<>();
        if (!StringUtils.isEmpty(ids)){
           idsList = Lists.newArrayList(ids.split(",")).stream().map(id->Integer.parseInt(id)).collect(Collectors.toList());
        }
        return osVersionMapper.deleteBatch(idsList);
    }

    @Override
    public OsVersion find(OsVersion osVersion) {
        return osVersionMapper.query(osVersion);
    }

    @Override
    public List<OsVersion> findAll(OsVersion osVersion) {
        return osVersionMapper.queryAll(osVersion);
    }

    @Override
    public List<OsVersion> findBatch(OsVersion osVersion) {
        return osVersionMapper.queryBatch(osVersion);
    }

    @Override
    public int findTotal(OsVersion osVersion) {
        return osVersionMapper.queryTotal(osVersion);
    }
}
