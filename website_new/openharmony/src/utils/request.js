import axios from 'axios'
import { Message } from 'element-ui'

const http = axios.create({
  baseURL: 'http://139.159.252.23:9066'
})

// 拦截请求
http.interceptors.request.use(config => {
  return config
}, err => {
  Promise.reject(err.message)
})

// 拦截响应
http.interceptors.response.use(response => {
  return response
}, err => {
  if (err.response) {
    console.log(err.response)
    Message({
      type: "err",
      message: err.response.data.message
    })
  }
  Promise.reject(err)
})

export default http