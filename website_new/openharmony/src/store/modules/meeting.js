import { queryMeeting } from "../../api/index"
import moment from "moment";
export default {
  state: {
    meetingList: [],
    moreList: []
  },
  getters: {

  },
  mutations: {
    getMeetingData (state, data) {
      state.meetingList = data;
      console.log(state.meetingList);
    },
    getMoreData (state, { list, meetList }) {
      // console.log("我是list", list);
      let time = new Date();
      let currentTime = moment(time).format("YYYY-MM-DD hh:mm");
      let newTime = new Date(currentTime.replace(/-/g, '/')).getTime();
      state.meetingList = meetList;
      list[0].data.forEach((item) => {
        let itemStart = item.date.concat(" ").concat(item.startTime);
        let itemLiTi = new Date(itemStart.replace(/-/g, '/')).getTime();
        console.log("kkkkkkkkkkkkkk", newTime, itemLiTi);
        if (newTime > itemLiTi) {
          item["flag"] = true;
        } else if (newTime === itemLiTi) {
          item["flag"] = true;
        } else {
          item["flag"] = false;
        }
      });
      state.moreList = list;
      console.log("item.list", state.moreList[0].data)
    },
  },
  actions: {
    async getMeetingData ({ commit }, date) {

      const { data } = await queryMeeting(date);
      if (data.code === 0) {
        // console.log("你好", data);
        commit("getMeetingData", data.data);
      }
    },
    async getMoreData ({ commit }, { date, currentDate }) {
      const { data } = await queryMeeting({ date: date });
      // console.log("我是更多", data.data);
      const meetList = data.data;
      if (data.code === 0) {
        let list = data.data.filter((item) => {
          return item.date === currentDate;
        })
        console.log("list", list);
        /* date: "2021-12-22"
        endTime: "18:00"
        id: (...)
        label: (...)
        name: (...)
        recordUrl: (...)
        startTime: "16:30" */

        commit("getMoreData", { list, meetList });
      }
    }
  }
}