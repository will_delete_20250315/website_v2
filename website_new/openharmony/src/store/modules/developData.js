import { queryDevelopmentBatch } from "../../api/index"
export default {
    state: {
        //开发板
        developmentBatchList: [],
        totalPage: 0
    },
    getters: {

    },
    mutations: {
        getQueryDevelopmentBatch(state, { list, totalPage }) {
            state.totalPage = totalPage;
            state.developmentBatchList = list;
            console.log(state.queryBatchList);
        },

    },
    actions: {
        async getQueryDevelopmentBatch({ commit }, { pageNum, pageSize }) {
            // let type = bannerData;
            // console.log("类型", type);
            const { data } = await queryDevelopmentBatch({ pageNum, pageSize });
            if (data.code === 0) {
                console.log("类型", data.data);
                commit("getQueryDevelopmentBatch", { list: data.data, totalPage: data.totalPage });
            }
        },

    }
}