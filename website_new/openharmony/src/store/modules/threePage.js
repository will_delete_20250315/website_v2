import { queryDetails } from "../../api/index"
export default {
    state: {
        queryDetailsList: [],
    },
    getters: {

    },
    mutations: {
        getQueryDetails(state, data) {
            state.queryDetailsList = data;
            console.log("state.queryDetailsList", state.queryDetailsList);
        },

    },
    actions: {
        async getQueryDetails({ commit }, id) {
            const { data } = await queryDetails(id);
            if (data.code === 0) {
                // console.log("你好", data);
                commit("getQueryDetails", data.data);
            }
        },

    }
}