import { queryBatch } from "../../api/index"
import moment from "moment";
export default {
  state: {
    //活动
    actBatchList: [],
    //博客
    blgBatchList: [],
    //新闻
    newBatchList: [],
    //直播
    liveBatchList: [],
    //视频
    videoBatchList: [],
    totalPage: 0
  },
  getters: {

  },
  mutations: {
    getQueryBatch (state, { list, type, totalPage }) {
      state.totalPage = totalPage;
      let time = new Date();
      let tempTime = moment(time).format("YYYY-MM-DD");
      let currentTime = moment(time).format("YYYY-MM-DD hh:mm");
      let newTime = new Date(currentTime.replace(/-/g, '/')).getTime();
      let yearTime = new Date(tempTime.replace(/-/g, '/')).getTime();
      switch (type) {
        case 1:
        case "1":
          state.actBatchList = list;
          console.log("state.actBatchList", state.actBatchList)
          break;
        case 2:
        case "2":
          state.blgBatchList = list;
          break;
        case 3:
        case "3":
          state.newBatchList = list;

          break;
        case 4:
        case "4":
          list.forEach((item) => {
            let temp3 = item.startTime.replace(".", "-").replace(".", "-");
            let itemLiTi = new Date(temp3.replace(/-/g, '/')).getTime();
            if (item.endTime) {
              let temp1 = item.endTime.split('-')[0];
              let temp2 = item.endTime.split('-')[1];
              let itemStart = item.startTime.replace(".", "-").replace(".", "-").concat(" ").concat(temp1);
              let itemEnd = item.startTime.replace(".", "-").replace(".", "-").concat(" ").concat(temp2);
              let itemLiHo1 = new Date(itemStart.replace(/-/g, '/')).getTime();
              let itemLiHo2 = new Date(itemEnd.replace(/-/g, '/')).getTime();
              console.log("newTime > itemLiHo2", newTime, itemLiHo2, itemStart, itemEnd, temp3)
              if (newTime >= itemLiHo2) {
                //已结束
                item["flag"] = 1;
              } else if (newTime >= itemLiHo1 && newTime < itemLiHo2) {
                //进行中
                item["flag"] = 2;
              } else if (newTime < itemLiHo1) {
                //待开始
                item["flag"] = 3;
              }
            } else {
              if (yearTime > itemLiTi) {
                //已结束
                item["flag"] = 1;
              } else if (yearTime === itemLiTi) {
                //进行中
                item["flag"] = 2;
              } else {
                //待开始
                item["flag"] = 3;
              }
            }
          });
          state.liveBatchList = list;
          console.log("listlistlist", state.liveBatchList, currentTime);
          break;
        case 5:
        case "5":
          state.videoBatchList = list;
          break;
        default:
          break;
      }

      console.log(state.queryBatchList);
    },

  },
  actions: {
    async getQueryBatch ({ commit }, { type, pageNum, pageSize }) {
      // let type = bannerData;
      // console.log("类型", type);
      const { data } = await queryBatch({ type, pageNum, pageSize });
      if (data.code === 0) {
        commit("getQueryBatch", { list: data.data, type, totalPage: data.totalPage });
      }
    },

  }
}