export default {
  state: {
    breadData: [
      { name: "学习", val: "" },
      { name: "开发样例", val: "" },
      { name: "应用开发", type: "title", titleId: "1", sort: 1 },
      { name: "UI", type: "bustype", busTypeId: "UI" },
    ],
  },
  mutations: {
    setBreadcrumb(state, item) {
      let data = state.breadData;
      let index = 0;
      for (let i = 0; i < data.length; i++) {
        if (data[i].type === item.type) {
          index = i;
          break;
        }
      }
      if (index) data = data.splice(0, index);
      data.push(item);
      state.breadData = data;
    },
  },
  actions: {
    setBreadcrumb({ commit }, item) {
      commit("setBreadcrumb", item);
    },
  },
};
