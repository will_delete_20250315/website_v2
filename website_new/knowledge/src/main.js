import Vue from "vue";
import App from "./App.vue";
import axios from "axios";

Vue.config.productionTip = false;
import "./assets/less/common.less";
import "./assets/less/font.less";
import ElementUI from "element-ui";
import store from "./store/store"
import "element-ui/lib/theme-chalk/index.css";
import "./assets/style/font/iconfont.css";
import "amfe-flexible"
import "./utils/amfe-flexible"
Vue.use(ElementUI);
// Vue.use(Pagination);
// axios.defaults.baseURL = "https://garden.openatom.cn:8188/sign"; //云
// axios.defaults.baseURL = "http://10.55.20.48:8188/sign"; //后台
axios.defaults.baseURL = "http://139.159.252.23:9066"; //后台
// axios.defaults.baseURL = "https://10.55.21.106:8188/sign";
// axios.defaults.baseURL = "http:/127.0.0.1:9088/autotool/"; //远程
Vue.prototype.$axios = axios;
import utils from "@/utils/utils.js";
Vue.prototype.$utils = utils;

// 控制器
// import Eruda from 'eruda';
// Eruda.init();

import router from "./router";

axios.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);
// 响应请求拦截器

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);
new Vue({
  render: (h) => h(App),
  router,
  store: store
}).$mount("#app");
